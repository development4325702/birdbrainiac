package com.Bird.Brainiac.secrets.universe.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Bird.Brainiac.secrets.universe.app.databinding.ActivityFieldThrushdBinding

class FieldThrushdActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFieldThrushdBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFieldThrushdBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        var correctAnswer = intent.getIntExtra("score", 0)


        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button3.isClickable = false
            binding.button4.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.red))
            binding.button3.isClickable = false
            binding.button1.isClickable = false
            binding.button4.isClickable = false

        }

        binding.button3.setOnClickListener {
            binding.button3.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button1.isClickable = false
            binding.button4.isClickable = false

        }

        binding.button4.setOnClickListener {
            binding.button4.setBackgroundColor(resources.getColor(R.color.green))
            correctAnswer++
            binding.button3.isClickable = false
            binding.button2.isClickable = false
            binding.button1.isClickable = false
        }

        binding.nextBtn.setOnClickListener {
            val intent = Intent(this, WaxwingActivity::class.java)
            intent.putExtra("score", correctAnswer)
            startActivity(intent)
            finish()
        }
        binding.hamb.setOnClickListener {
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}
