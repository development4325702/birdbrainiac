package com.Bird.Brainiac.secrets.universe.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Bird.Brainiac.secrets.universe.app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setBirdFullScreen()

        binding.startBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, GreatGrayOwlActivity::class.java)
            startActivity(intent)
        }

        binding.exitBtn.setOnClickListener {
            finish()
        }
    }
    private fun setBirdFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}