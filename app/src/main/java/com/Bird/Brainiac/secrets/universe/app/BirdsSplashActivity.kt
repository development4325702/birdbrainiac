package com.Bird.Brainiac.secrets.universe.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Bird.Brainiac.secrets.universe.app.databinding.ActivityBirdsSplashBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BirdsSplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBirdsSplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBirdsSplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        GlobalScope.launch {
            delay(2900)
            val intent = Intent(this@BirdsSplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}